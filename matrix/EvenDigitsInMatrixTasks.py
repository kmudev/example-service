import numpy as np
from enum import Enum

class Operaton(Enum):
    CalculateDeterminant = 1
    TransposeMatrix = 2
    MultiplyMatrices = 3
    AddMatrices = 4
    SybstractMatrices = 5
    MultiplyMatrixByScalar = 6
    MatrixRank = 7
    LinearEquations = 8
    InverseMatrix = 9
    

def read_matrix(R,C):
    matrix = []
    for i in range(R):          
        a = []
        for j in range(C):     
             a.append(int(input()))
        matrix.append(a)
    return matrix;

def print_matrix(R,C,matrix):
    for i in range(R):
        for j in range(C):
            print(matrix[i][j], end = " ")
        print()

def transpose_matrix(matrix):
    return np.transpose(matrix);

def add_matrices(A,B):
    return np.add(A,B)

def multiply_with_Scalar(matrix,scalar):
    return np.array(matrix) * scalar

def linear_solve(A,B):
    return np.linalg.solve(A,B)