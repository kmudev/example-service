from flask import Flask, redirect, url_for, request
from datetime import datetime
from markupsafe import escape

app = Flask(__name__)

@app.route('/pg', methods=['GET'])
def pg():
  return "<h1>PG </h1>"

@app.route("/krisi", methods=["GET", "POST"])
def root():
    return "<h1>Krisi</h1>"

@app.route("/denytsa", methods=["GET", "POST"])
def root():
    return "<h1>Test test</h1>"

@app.route("/first", methods=["GET", "POST"])
def root():
    if request.method == "POST":
        # If the form is submitted, get the entered name
        name = request.form.get("name", "")
        # Redirect to the personalized greeting page
        return redirect(url_for("greet", name=name))
    else:
        # If it's a GET request, display the input form
        return """
        <h1>Hello, World!</h1>
        <form action="/" method="post">
            <label for="name">Enter your name:</label>
            <input type="text" id="name" name="name" required>
            <input type="submit" value="Submit">
        </form>
        <form action="/current_time">
            <input type="submit" value="Get Current Time">
        </form>
        """

@app.route("/greet/<name>")
def greet(name):
    return "<h1>Hello, World!1!</h1>"

@app.route("/<name>")
def hello(name):
    return f"Hello, {escape(name)}!"

@app.route("/current_time")
def current_time():
    now = datetime.now()
    formatted_time = now.strftime("%Y-%m-%d %H:%M:%S")
    return f"<h2>Current Time: {formatted_time}</h2>"


if __name__ == "__main__":
    app.run(debug=True)
