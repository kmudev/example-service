# example-service

## How to run it locally

### Prerequisites
Install the Flask module via:\
`pip install -r requirements.txt`

### Run the app
Execute the following command:\
`python -m flask run`
 