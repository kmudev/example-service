from sympy import symbols, diff, factorial, exp

x = symbols("x")

n = 20
x0 = 0
func = exp(x)
result = func.subs(x, x0)

for i in range(1, n):
    result += diff(func, x, i).subs(x, x0) * ((x - x0)**i)/(factorial(i))

print(result)

